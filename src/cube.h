/**
 * Max Control
 * Copyright (c) 2015 Jan-Michael Brummer
 *
 * This file is part of Max Control.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CUBE_H
#define CUBE_H

#include <glib.h>
#include <gio/gio.h>

#include <libintl.h>

#define _(x) gettext(x)

#define MULTICAST		"224.0.0.1"
#define MAX_UDP_PORT		23272
#define MAX_TCP_PORT		62910

#define INCOMING_HELLO				'H'
#define INCOMING_METADATA			'M'
#define INCOMING_CONFIGURATION			'C'
#define INCOMING_DEVICE_LIST			'L'
#define INCOMING_ENCRYPTION			'E'
#define INCOMING_NEW_DEVICE			'N'
#define INCOMING_ACKNOWLEDGE			'A'
#define INCOMING_SEND_DEVICE_CMD 		'S'
#define INCOMING_DECRYPTION	 		'D'
#define INCOMING_CHECK_PRODUCT_ACTIVATION	'v'
#define INCOMING_GET_CREDENTIALS		'g'

#define CUBE_CMD_CONFIG_WEEK_PROFILE		0x10
#define CUBE_CMD_CONFIG_TEMPERATURE		0x11
#define CUBE_CMD_CONFIG_VALVE			0x12
#define CUBE_CMD_ADD_LINK_PARTNER		0x20
#define CUBE_CMD_REMOVE_LINK_PARTNER		0x21
#define CUBE_CMD_SET_GROUP_ID			0x22
#define CUBE_CMD_SET_TEMPERATURE		0x40
#define CUBE_CMD_SET_DISPLAY_ACTUAL_TEMP	0x82

typedef enum {
	CUBE_DEVICE_TYPE_CUBE		= 0x00,
	CUBE_DEVICE_TYPE_HEATING	= 0x01,
	CUBE_DEVICE_TYPE_HEATING_PLUS	= 0x02,
	CUBE_DEVICE_TYPE_WALL		= 0x03,
	CUBE_DEVICE_TYPE_SHUTTER	= 0x04,
	CUBE_DEVICE_TYPE_BUTTON		= 0x05,
} eCubeDeviceType;

typedef enum {
	CUBE_MODE_AUTO		= 0x00,
	CUBE_MODE_MANUAL	= 0x01,
	CUBE_MODE_VACATION	= 0x02,
	CUBE_MODE_BOOST		= 0x03
} eCubeMode;

struct cube_detect_header {
	GString *response;
	GString *serial;
	GString *reserved;
	gint rf_address;
	gint fw_version;
};

struct cube_header
{
	/* Serial number */
	gchar *serial_number;
	/* RF address */
	gchar *rf_address;
	/* Firmware version */
	gchar *fw_version;
	/* Unknown */
	gchar *unknown;
	/* HTTP Connect ID? */
	gchar *http_connect_id;
	/* Duty cycle? */
	gchar *duty_cycle;
	/* Free memory slots */
	gchar *free_memory_slots;
	/* Date */
	gchar *cube_date;
	/* Time */
	gchar *cube_time;
	/* Clock set? */
	gchar *state_cube_time;
	/* NTP Counter? */
	gchar *ntp_counter;
};

struct cube_room {
	gint id;
	GString *name;
	gint rf_address;

	struct cube *cube;

	/* Devices */
	GList *devices;

	gpointer priv;
};

struct cube_day_entry {
	gfloat deg;
	gint min;
};

struct cube_device_config {
	gint rf_address;
	gint type;
	gint unknown;
	GString *serial_number;
	gfloat comfort_temp;
	gfloat eco_temp;
	/* Max. Temp 30.5 => unset */
	gfloat max_set_point_temp;
	/* Min. Temp 4.5 => unset */
	gfloat min_set_point_temp;
	gfloat temp_offset;
	gfloat window_open_temp;
	gint window_open_duration;
	gint boost_duration_val;
	gint decalcification;
	gint max_val_setting;
	gint val_offset;

	/* Weekly... */
	GList *days[7];
};

struct cube_config {
	gint portal_enabled;
	GString *portal_url;
};

struct cube_device {
	gint type;
	gint rf_address;
	GString *serial_number;
	GString *name;
	gint room_id;
	gint cube_rf_address;

	struct cube_device_config *config;

	/* Link status */
	gint status1;
	gint status2;
	gint valve;
	gfloat set_temp;
	gint date_until;
	gint time_until;

	gfloat current_temp;
};

struct cube {
	/* Network Address */
	GInetAddress *address;
	GSocket *socket;
	GMutex tx_mutex;
	gboolean ack;
	GCond cond;
	gchar wait_for;

	/* Detect Header */
	struct cube_detect_header detect_header;
	/* Header */
	struct cube_header header;
	/* Config */
	struct cube_config *config;
	/* Rooms */
	GList *rooms;
	GList *devices;

	/* Transmission timeout timer */
	GTimer *timer;

	gint remaining_days;
	gchar *end_date;
	gchar *user;
	gboolean internet_control;
};

struct cube_metadata {
	gchar index[2];
	gchar col0;
	gchar count[2];
	gchar col1;
	guchar *data;
};

/* Cube functions */
GList *cube_detect(gchar *ip);
void cube_set_active(struct cube *cube);
struct cube *cube_get_active(void);
void cube_start_thread(struct cube *cube);
gboolean cube_check_product_activation(struct cube *cube);
gboolean cube_start_device_scan(struct cube *cube);
gboolean cube_stop_device_scan(struct cube *cube);
gboolean cube_send_meta(struct cube *cube);
gboolean cube_rename_room(struct cube *cube, struct cube_room *room, const gchar *name);
gboolean cube_rename_device(struct cube *cube, struct cube_device *device, const gchar *name);
gboolean cube_remove_room(struct cube *cube, struct cube_room *room);
gboolean cube_send_wakeup(struct cube *cube);
gboolean cube_add_room(struct cube *cube, const gchar *name);
gboolean cube_send_device_base_config(struct cube *cube, struct cube_device *device);
gboolean cube_set_wall_display(struct cube *cube, struct cube_device *device, gint value);
struct cube_device *cube_get_device_by_address(struct cube *cube, gint rf_address);
struct cube_room *cube_create_room(struct cube *cube, const gchar *name);
struct cube_room *cube_get_room_by_name(struct cube *cube, const gchar *name);
struct cube_room *cube_get_room_by_id(struct cube *cube, gint id);
gint cube_get_free_room_id(struct cube *cube);
gchar *cube_propose_device_name(struct cube *cube, struct cube_device *device);
gboolean cube_set_device_temperature(struct cube *cube, struct cube_device *device, eCubeMode mode, gfloat temperature, gint until_date, gint until_time);
gint cube_compute_date(gint day, gint month, gint year);
gint cube_compute_time(gint hour, gint min);
void cube_get_date(gint until_date, gint *day, gint *month, gint *year);
void cube_get_time(gint until_time, gint *hour, gint *min);
gboolean cube_get_credentials(struct cube *cube);

/* Global */
gchar *cube_device_string(eCubeDeviceType type);
gboolean cube_shutdown(struct cube *cube);
void cube_init(void);

/* Room functions */
void cube_room_remove_device(struct cube_room *room, struct cube_device *device);
void cube_room_add_device(struct cube_room *room, struct cube_device *device);
struct cube_device *cube_room_get_device(struct cube_room *room, eCubeDeviceType type);
gboolean cube_room_set_base_config(struct cube_room *room, gfloat eco, gfloat comfort, gfloat window, gfloat max_set_point);
gboolean cube_room_config_valve(struct cube_room *room, gint boost, gint valve, gint decalc_day, gint decalc_hour, gint max, gint offset);

/* Device functions */
eCubeMode cube_device_get_mode(struct cube_device *device);
gboolean cube_device_is_battery_low(struct cube_device *device);
gboolean cube_device_request_config(struct cube *cube, struct cube_device *device);
gboolean cube_send_device_week_program(struct cube *cube, struct cube_device *device);
gboolean cube_send_device_day_program(struct cube *cube, struct cube_device *device, gint day, GList *program);

#endif
