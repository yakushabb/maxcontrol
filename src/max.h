/**
 * Max Control
 * Copyright (c) 2015 Jan-Michael Brummer
 *
 * This file is part of Max Control.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 only.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAX_H
#define MAX_H

#include <libintl.h>

#define _(x) gettext(x)

void max_rename_room(GSimpleAction *action, GVariant *parameter, gpointer user_data);
void max_remove_room(GSimpleAction *action, GVariant *parameter, gpointer user_data);
void max_remove_device(GSimpleAction *action, GVariant *parameter, gpointer user_data);
void max_rename_device(GSimpleAction *action, GVariant *parameter, gpointer user_data);

#endif
